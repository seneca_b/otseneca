local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

local count = {}
local transfer = {}

function onCreatureAppear(cid)
npcHandler:onCreatureAppear(cid)
end
function onCreatureDisappear(cid)
npcHandler:onCreatureDisappear(cid)
end
function onCreatureSay(cid, type, msg)
npcHandler:onCreatureSay(cid, type, msg)
end
function onThink()
npcHandler:onThink()
end

local voices = { {text = 'Nao se esqueca de depositar seu dinheiro aqui no Global Bank antes de sair pra hunt!!'} }
--local voices = { {text = 'Don\'t forget to deposit your money here in the Global Bank before you head out for adventure.'} }
if VoiceModule then
	npcHandler:addModule(VoiceModule:new(voices))
end
--------------------------------guild bank-----------------------------------------------
local receiptFormat = 'Date: %s\nType: %s\nGold Amount: %d\nReceipt Owner: %s\nRecipient: %s\n\n%s'
local function getReceipt(info)
	local receipt = Game.createItem(info.success and 24301 or 24302)
	receipt:setAttribute(ITEM_ATTRIBUTE_TEXT, receiptFormat:format(os.date('%d. %b %Y - %H:%M:%S'), info.type, info.amount, info.owner, info.recipient, info.message))

	return receipt
end

local function getGuildIdByName(name, func)
	db.asyncStoreQuery('SELECT `id` FROM `guilds` WHERE `name` = ' .. db.escapeString(name),
		function(resultId)
			if resultId then
				func(result.getNumber(resultId, 'id'))
				result.free(resultId)
			else
				func(nil)
			end
		end
	)
end

local function getGuildBalance(id)
	local guild = Guild(id)
	if guild then
		return guild:getBankBalance()
	else
		local balance
		local resultId = db.storeQuery('SELECT `balance` FROM `guilds` WHERE `id` = ' .. id)
		if resultId then
			balance = result.getNumber(resultId, 'balance')
			result.free(resultId)
		end

		return balance
	end
end

local function setGuildBalance(id, balance)
	local guild = Guild(id)
	if guild then
		guild:setBankBalance(balance)
	else
		db.query('UPDATE `guilds` SET `balance` = ' .. balance .. ' WHERE `id` = ' .. id)
	end
end

local function transferFactory(playerName, amount, fromGuildId, info)
	return function(toGuildId)
		if not toGuildId then
			local player = Player(playerName)
			if player then
				info.success = false
				info.message = 'Lamentamos informá-lo que nao podemos atender sua solicitacao, pois nao encontramos a guilda destinada.'
				local inbox = player:getInbox()
				local receipt = getReceipt(info)
				inbox:addItemEx(receipt, INDEX_WHEREEVER, FLAG_NOLIMIT)
			end
		else
			local fromBalance = getGuildBalance(fromGuildId)
			if fromBalance < amount then
				info.success = false
				info.message = 'Lamentamos informar que nao podemos atender sua solicitacao, devido a falta da quantia necessária em sua conta da guilda.'
			else
				info.success = true
				info.message = 'Temos o prazer de informar que sua solicitacao de transferência foi realizada com sucesso.'
				setGuildBalance(fromGuildId, fromBalance - amount)
				setGuildBalance(toGuildId, getGuildBalance(toGuildId) + amount)
			end

			local player = Player(playerName)
			if player then
				local inbox = player:getInbox()
				local receipt = getReceipt(info)
				inbox:addItemEx(receipt, INDEX_WHEREEVER, FLAG_NOLIMIT)
			end
		end
	end
end
--------------------------------guild bank-----------------------------------------------

local function greetCallback(cid)
	count[cid], transfer[cid] = nil, nil
	return true
end

local function creatureSayCallback(cid, type, msg)
	if not npcHandler:isFocused(cid) then
		return false
	end
	local player = Player(cid)
---------------------------- help ------------------------
	if msgcontains(msg, 'bank account') then
		npcHandler:say({
			'Todo cidadão tem um. A grande vantagem eh que Voce pode acessar seu dinheiro em todas as agencias do Global! ...',
			'Voce gostaria de saber mais sobre a {basic} function do seu banco, a {advanced} function, ou voce esta entendiado, talvez?'
		}, cid)
		npcHandler.topic[cid] = 0
		return true
---------------------------- balance ---------------------
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'guild balance') then
		npcHandler.topic[cid] = 0
		if not player:getGuild() then
			npcHandler:say('Voce nao eh membro de uma guilda!', cid)
			return false
		end
		npcHandler:say('O saldo da sua conta eh de' .. player:getGuild():getBankBalance() .. ' gold.', cid)
		return true
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'balance') then
		npcHandler.topic[cid] = 0
		if player:getBankBalance() >= 100000000 then
			npcHandler:say('Acho que Voce deve ser um dos habitantes mais ricos do mundo! O saldo da sua conta eh ' .. player:getBankBalance() .. ' gold.', cid)
			return true
		elseif player:getBankBalance() >= 10000000 then
			npcHandler:say('Voce fez dez milhões e ainda cresce! O saldo da sua conta eh' .. player:getBankBalance() .. ' gold.', cid)
			return true
		elseif player:getBankBalance() >= 1000000 then
			npcHandler:say('Uau, Voce atingiu o número mágico de um milhão de gp!!! O saldo da sua conta eh' .. player:getBankBalance() .. ' gold!', cid)
			return true
		elseif player:getBankBalance() >= 100000 then
			npcHandler:say('Voce certamente fez um belo centavo. O saldo da sua conta eh' .. player:getBankBalance() .. ' gold.', cid)
			return true
		else
			npcHandler:say('O saldo da sua conta eh de ' .. player:getBankBalance() .. ' gold.', cid)
			return true
		end
---------------------------- deposit ---------------------
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'guild deposit') then
		if not player:getGuild() then
			npcHandler:say('Voce nao eh menbro de uma guilda!', cid)
			npcHandler.topic[cid] = 0
			return false
		end
	   -- count[cid] = player:getMoney()
	   -- if count[cid] < 1 then
		   -- npcHandler:say('Voce nao tem dinheiro suficente.', cid)
		   -- npcHandler.topic[cid] = 0
		   -- return false
		--end
		if string.match(msg, '%d+') then
			count[cid] = getMoneyCount(msg)
			if count[cid] < 1 then
				npcHandler:say('Voce nao tem dinheiro suficente.', cid)
				npcHandler.topic[cid] = 0
				return false
			end
			npcHandler:say('Voce gostaria de depositar ' .. count[cid] .. ' gold na sua {guild account}?', cid)
			npcHandler.topic[cid] = 23
			return true
		else
			npcHandler:say('Por favor, diga-me quanto ouro Voce gostaria de depositar.', cid)
			npcHandler.topic[cid] = 22
			return true
		end
	elseif npcHandler.topic[cid] == 22 then
		count[cid] = getMoneyCount(msg)
		if isValidMoney(count[cid]) then
			npcHandler:say('Voce gostaria de depositar ' .. count[cid] .. ' gold na sua {guild account}?', cid)
			npcHandler.topic[cid] = 23
			return true
		else
			npcHandler:say('Voce nao tem dinheiro suficente.', cid)
			npcHandler.topic[cid] = 0
			return true
		end
	elseif npcHandler.topic[cid] == 23 then
		if msgcontains(msg, 'yes') then
			npcHandler:say('Certo, fizemos um pedido para depositar o valor de ' .. count[cid] .. ' gold na sua guild account. Por favor, verifique sua caixa de entrada para confirmação.', cid)
			local guild = player:getGuild()
			local info = {
				type = 'Guild Deposit',
				amount = count[cid],
				owner = player:getName() .. ' of ' .. guild:getName(),
				recipient = guild:getName()
			}
			local playerBalance = player:getBankBalance()
			if playerBalance < tonumber(count[cid]) then
				info.message = 'Lamentamos informá-lo que nao podemos atender sua solicitacao, devido a falta do valor necessário em sua conta bancaria.'
				info.success = false
			else
				info.message = 'Temos o prazer de informar que sua solicitacao de transferência foi realizada com sucesso.'
				info.success = true
				guild:setBankBalance(guild:getBankBalance() + tonumber(count[cid]))
				player:setBankBalance(playerBalance - tonumber(count[cid]))
			end

			local inbox = player:getInbox()
			local receipt = getReceipt(info)
			inbox:addItemEx(receipt, INDEX_WHEREEVER, FLAG_NOLIMIT)
		elseif msgcontains(msg, 'no') then
			npcHandler:say('Como quiser. Existe algo mais que eu possa fazer por Voce?', cid)
		end
		npcHandler.topic[cid] = 0
		return true
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'deposit') then
		count[cid] = player:getMoney()
		if count[cid] < 1 then
			npcHandler:say('Voce nao tem dinheiro suficente.', cid)
			npcHandler.topic[cid] = 0
			return false
		end
		if msgcontains(msg, 'all') then
			count[cid] = player:getMoney()
			npcHandler:say('Voce gostaria de depositar ' .. count[cid] .. ' gold?', cid)
			npcHandler.topic[cid] = 2
			return true
		else
			if string.match(msg,'%d+') then
				count[cid] = getMoneyCount(msg)
				if count[cid] < 1 then
					npcHandler:say('Voce nao tem dinheiro suficente.', cid)
					npcHandler.topic[cid] = 0
					return false
				end
				npcHandler:say('Voce gostaria de depositar ' .. count[cid] .. ' gold?', cid)
				npcHandler.topic[cid] = 2
				return true
			else
				npcHandler:say('Por favor, diga-me quanto ouro Voce gostaria de depositar.', cid)
				npcHandler.topic[cid] = 1
				return true
			end
		end
		if not isValidMoney(count[cid]) then
			npcHandler:say('Desculpe, but Voce pode\'t deposit that much.', cid)
			npcHandler.topic[cid] = 0
			return false
		end
	elseif npcHandler.topic[cid] == 1 then
		count[cid] = getMoneyCount(msg)
		if isValidMoney(count[cid]) then
			npcHandler:say('Voce gostaria de depositar ' .. count[cid] .. ' gold?', cid)
			npcHandler.topic[cid] = 2
			return true
		else
			npcHandler:say('Voce nao tem dinheiro suficente.', cid)
			npcHandler.topic[cid] = 0
			return true
		end
	elseif npcHandler.topic[cid] == 2 then
		if msgcontains(msg, 'yes') then
			if player:depositMoney(count[cid]) then
				npcHandler:say('Certo, adicionamos a quantidade de ' .. count[cid] .. ' gold na sua {balance}. Voce pode {withdraw} seu dinheiro sempre que quiser.', cid)
			else
				npcHandler:say('Voce nao tem dinheiro suficente.', cid)
			end
		elseif msgcontains(msg, 'no') then
			npcHandler:say('Como quiser. Existe algo mais que eu possa fazer por Voce?', cid)
		end
		npcHandler.topic[cid] = 0
		return true
---------------------------- withdraw --------------------
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'guild withdraw') then
		if not player:getGuild() then
			npcHandler:say('Sinto muito, mas parece que Voce nao está atualmente em nenhuma guilda.', cid)
			npcHandler.topic[cid] = 0
			return false
		elseif player:getGuildLevel() < 2 then
			npcHandler:say('Apenas líderes ou vice-líderes da guilda podem retirar dinheiro da conta da guilda.', cid)
			npcHandler.topic[cid] = 0
			return false
		end

		if string.match(msg,'%d+') then
			count[cid] = getMoneyCount(msg)
			if isValidMoney(count[cid]) then
				npcHandler:say('Tem certeza de que deseja retirar ' .. count[cid] .. ' gold da sua conta da guilda?', cid)
				npcHandler.topic[cid] = 25
			else
				npcHandler:say('nao há ouro suficiente na sua conta da guilda.', cid)
				npcHandler.topic[cid] = 0
			end
			return true
		else
			npcHandler:say('Por favor me diga quanto ouro Voce gostaria de retirar da sua conta da guilda.', cid)
			npcHandler.topic[cid] = 24
			return true
		end
	elseif npcHandler.topic[cid] == 24 then
		count[cid] = getMoneyCount(msg)
		if isValidMoney(count[cid]) then
			npcHandler:say('Tem certeza de que deseja retirar ' .. count[cid] .. ' gold da sua conta da guilda?', cid)
			npcHandler.topic[cid] = 25
		else
			npcHandler:say('nao há ouro suficiente na sua conta da guilda.', cid)
			npcHandler.topic[cid] = 0
		end
		return true
	elseif npcHandler.topic[cid] == 25 then
		if msgcontains(msg, 'yes') then
			local guild = player:getGuild()
			local balance = guild:getBankBalance()
			npcHandler:say('Fizemos um pedido para retirar ' .. count[cid] .. ' gold da sua conta da guilda. Por favor, verifique sua caixa de entrada para confirmação.', cid)
			local info = {
				type = 'Guild Withdraw',
				amount = count[cid],
				owner = player:getName() .. ' of ' .. guild:getName(),
				recipient = player:getName()
			}
			if balance < tonumber(count[cid]) then
				info.message = 'Lamentamos informar que nao podemos atender sua solicitacao, devido a falta da quantia necessária em sua conta da guilda.'
				info.success = false
			else
				info.message = 'Temos o prazer de informar que sua solicitacao de transferência foi realizada com sucesso.'
				info.success = true
				guild:setBankBalance(balance - tonumber(count[cid]))
				local playerBalance = player:getBankBalance()
				player:setBankBalance(playerBalance + tonumber(count[cid]))
			end

			local inbox = player:getInbox()
			local receipt = getReceipt(info)
			inbox:addItemEx(receipt, INDEX_WHEREEVER, FLAG_NOLIMIT)
			npcHandler.topic[cid] = 0
		elseif msgcontains(msg, 'no') then
			npcHandler:say('Como quiser. Existe algo mais que eu possa fazer por Voce?', cid)
			npcHandler.topic[cid] = 0
		end
		return true
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'retirar') then
		if string.match(msg,'%d+') then
			count[cid] = getMoneyCount(msg)
			if isValidMoney(count[cid]) then
				npcHandler:say('Tem certeza de que deseja retirar ' .. count[cid] .. ' gold da sua conta no banco?', cid)
				npcHandler.topic[cid] = 7
			else
				npcHandler:say('nao há ouro suficiente em sua conta.', cid)
				npcHandler.topic[cid] = 0
			end
			return true
		else
			npcHandler:say('Por favor me diga quanto ouro Voce gostaria de retirar.', cid)
			npcHandler.topic[cid] = 6
			return true
		end
	elseif npcHandler.topic[cid] == 6 then
		count[cid] = getMoneyCount(msg)
		if isValidMoney(count[cid]) then
			npcHandler:say('Tem certeza de que deseja retirar ' .. count[cid] .. ' gold da sua conta no banco?', cid)
			npcHandler.topic[cid] = 7
		else
			npcHandler:say('nao há ouro suficiente em sua conta.', cid)
			npcHandler.topic[cid] = 0
		end
		return true
	elseif npcHandler.topic[cid] == 7 then
		if msgcontains(msg, 'yes') then
			if player:getFreeCapacity() >= getMoneyWeight(count[cid]) then
				if not player:withdrawMoney(count[cid]) then
					npcHandler:say('nao há ouro suficiente em sua conta.', cid)
				else
					npcHandler:say('Pronto, aqui esta!! ' .. count[cid] .. ' gold. Please let me know if there is something else I can do for you.', cid)
				end
			else
				npcHandler:say('Uau, espere, Voce nao tem espaço em seu inventário para carregar todas essas moedas. Eu nao quero que Voce jogue no chão, talvez volte com um carrinho!', cid)
			end
			npcHandler.topic[cid] = 0
		elseif msgcontains(msg, 'no') then
			npcHandler:say('O cliente eh rei! Volte sempre que quiser, se desejar {withdraw} seu dinheiro.', cid)
			npcHandler.topic[cid] = 0
		end
		return true
---------------------------- transfer --------------------
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'guild transfer') then
		if not player:getGuild() then
			npcHandler:say('Sinto muito, mas parece que Voce nao está atualmente em nenhuma guilda.', cid)
			npcHandler.topic[cid] = 0
			return false
		elseif player:getGuildLevel() < 2 then
			npcHandler:say('Apenas líderes ou vice-líderes da guilda podem transferir dinheiro da conta da guilda.', cid)
			npcHandler.topic[cid] = 0
			return false
		end

		if string.match(msg, '%d+') then
			count[cid] = getMoneyCount(msg)
			if isValidMoney(count[cid]) then
				transfer[cid] = string.match(msg, 'to%s*(.+)$')
				if transfer[cid] then
					npcHandler:say('Então Voce gostaria de transferir ' .. count[cid] .. ' gold da sua conta da guilda to guild ' .. transfer[cid] .. '?', cid)
					npcHandler.topic[cid] = 28
				else
					npcHandler:say('Qual guilda Voce gostaria de transferir ' .. count[cid] .. ' gold to?', cid)
					npcHandler.topic[cid] = 27
				end
			else
				npcHandler:say('nao há ouro suficiente na sua conta da guilda.', cid)
				npcHandler.topic[cid] = 0
			end
		else
			npcHandler:say('Por favor, diga-me a quantidade de ouro que Voce gostaria de transferir.', cid)
			npcHandler.topic[cid] = 26
		end
		return true
	elseif npcHandler.topic[cid] == 26 then
		count[cid] = getMoneyCount(msg)
		if player:getGuild():getBankBalance() < count[cid] then
			npcHandler:say('nao há ouro suficiente na sua conta da guilda.', cid)
			npcHandler.topic[cid] = 0
			return true
		end
		if isValidMoney(count[cid]) then
			npcHandler:say('Qual guilda Voce gostaria de transferir ' .. count[cid] .. ' gold to?', cid)
			npcHandler.topic[cid] = 27
		else
			npcHandler:say('nao há ouro suficiente em sua conta.', cid)
			npcHandler.topic[cid] = 0
		end
		return true
	elseif npcHandler.topic[cid] == 27 then
		transfer[cid] = msg
		if player:getGuild():getName() == transfer[cid] then
			npcHandler:say('Preencha este campo com a pessoa que recebe seu ouro!', cid)
			npcHandler.topic[cid] = 0
			return true
		end
		npcHandler:say('Então Voce gostaria de transferir ' .. count[cid] .. ' gold da sua conta da guilda to guild ' .. transfer[cid] .. '?', cid)
		npcHandler.topic[cid] = 28
		return true
	elseif npcHandler.topic[cid] == 28 then
		if msgcontains(msg, 'yes') then
			npcHandler:say('We have placed an order to transfer ' .. count[cid] .. ' gold da sua conta da guilda to guild ' .. transfer[cid] .. '. Por favor, verifique sua caixa de entrada para confirmação.', cid)
			local guild = player:getGuild()
			local balance = guild:getBankBalance()
			local info = {
				type = 'Guild to Guild Transfer',
				amount = count[cid],
				owner = player:getName() .. ' of ' .. guild:getName(),
				recipient = transfer[cid]
			}
			if balance < tonumber(count[cid]) then
				info.message = 'Lamentamos informar que nao podemos atender sua solicitacao, devido a falta da quantia necessária em sua conta da guilda.'
				info.success = false
				local inbox = player:getInbox()
				local receipt = getReceipt(info)
				inbox:addItemEx(receipt, INDEX_WHEREEVER, FLAG_NOLIMIT)
			else
				getGuildIdByName(transfer[cid], transferFactory(player:getName(), tonumber(count[cid]), guild:getId(), info))
			end
			npcHandler.topic[cid] = 0
		elseif msgcontains(msg, 'no') then
			npcHandler:say('Tudo bem, há algo mais que eu possa fazer por voce?', cid)
		end
		npcHandler.topic[cid] = 0
--------------------------------guild bank-----------------------------------------------
	elseif msgcontains(msg, 'transfer') then
		npcHandler:say('Por favor, diga-me a quantidade de ouro que Voce gostaria de transferir.', cid)
		npcHandler.topic[cid] = 11
	elseif npcHandler.topic[cid] == 11 then
		count[cid] = getMoneyCount(msg)
		if player:getBankBalance() < count[cid] then
			npcHandler:say('nao há ouro suficiente em sua conta.', cid)
			npcHandler.topic[cid] = 0
			return true
		end
		if isValidMoney(count[cid]) then
			npcHandler:say('Quem Voce gostaria de transferir ' .. count[cid] .. ' gold para?', cid)
			npcHandler.topic[cid] = 12
		else
			npcHandler:say('nao há ouro suficiente em sua conta.', cid)
			npcHandler.topic[cid] = 0
		end
	elseif npcHandler.topic[cid] == 12 then
		transfer[cid] = msg
		if player:getName() == transfer[cid] then
			npcHandler:say('Preencha este campo com a pessoa que recebe seu ouro!', cid)
			npcHandler.topic[cid] = 0
			return true
		end
		if playerExists(transfer[cid]) then
		local arrayDenied = {"accountmanager", "rooksample", "druidsample", "sorcerersample", "knightsample", "paladinsample"}
			if isInArray(arrayDenied, string.gsub(transfer[cid]:lower(), " ", "")) then
				npcHandler:say('Este jogador nao existe.', cid)
				npcHandler.topic[cid] = 0
				return true
			end
			npcHandler:say('Então Voce gostaria de transferir ' .. count[cid] .. ' gold to ' .. transfer[cid] .. '?', cid)
			npcHandler.topic[cid] = 13
		else
			npcHandler:say('Este jogador nao existe.', cid)
			npcHandler.topic[cid] = 0
		end
	elseif npcHandler.topic[cid] == 13 then
		if msgcontains(msg, 'yes') then
			if not player:transferMoneyTo(transfer[cid], count[cid]) then
				npcHandler:say("Receio que este personagem tenha apenas uma conta júnior em nosso banco. nao se preocupe, no entanto. Uma vez que ele tenha escolhido sua vocação ou nao esteja mais em Dawnport, sua conta será atualizada.", cid)
			else
				npcHandler:say('Muito bem. voce transferiu ' .. count[cid] .. ' gold to ' .. transfer[cid] ..'.', cid)
				transfer[cid] = nil
			end
		elseif msgcontains(msg, 'no') then
			npcHandler:say('Tudo bem, há algo mais que eu possa fazer por voce?', cid)
		end
		npcHandler.topic[cid] = 0
---------------------------- money extrocar --------------
	elseif msgcontains(msg, 'converter gold') then
		npcHandler:say('Quantas moedas de platinum voce gostaria de obter?', cid)
		npcHandler.topic[cid] = 14
	elseif npcHandler.topic[cid] == 14 then
		if getMoneyCount(msg) < 1 then
			npcHandler:say('Desculpe, mas voce nao tem dinheiro suficente.', cid)
			npcHandler.topic[cid] = 0
		else
			count[cid] = getMoneyCount(msg)
			npcHandler:say('Então Voce gostaria que eu mudasse ' .. count[cid] * 100 .. ' de suas moedas de ouro em ' .. count[cid] .. ' platinum coins?', cid)
			npcHandler.topic[cid] = 15
		end
	elseif npcHandler.topic[cid] == 15 then
		if msgcontains(msg, 'yes') then
			if player:removeItem(2148, count[cid] * 100) then
				player:addItem(2152, count[cid])
				npcHandler:say('Pronto, aqui esta!!', cid)
			else
				npcHandler:say('Desculpe, mas voce nao tem dinheiro suficente coins.', cid)
			end
		else
			npcHandler:say('Bem, posso ajuda-lo com outra coisa?', cid)
		end
		npcHandler.topic[cid] = 0
	elseif msgcontains(msg, 'converter platinum') then
		npcHandler:say('Voce gostaria de mudar suas moedas de platinum em god ou crystal?', cid)
		npcHandler.topic[cid] = 16
	elseif npcHandler.topic[cid] == 16 then
		if msgcontains(msg, 'gold') then
			npcHandler:say('Quantas moedas de platinum Voce gostaria de transformar em ouro?', cid)
			npcHandler.topic[cid] = 17
		elseif msgcontains(msg, 'crystal') then
			npcHandler:say('Quantas moedas de crystal Voce gostaria de obter?', cid)
			npcHandler.topic[cid] = 19
		else
			npcHandler:say('Bem, posso ajuda-lo com outra coisa?', cid)
			npcHandler.topic[cid] = 0
		end
	elseif npcHandler.topic[cid] == 17 then
		if getMoneyCount(msg) < 1 then
			npcHandler:say('Desculpe, mas voce nao tem moedas de platinum suficientes.', cid)
			npcHandler.topic[cid] = 0
		else
			count[cid] = getMoneyCount(msg)
			npcHandler:say('Então Voce gostaria que eu mudasse ' .. count[cid] .. ' of your platinum coins into ' .. count[cid] * 100 .. ' gold coins for you?', cid)
			npcHandler.topic[cid] = 18
		end
	elseif npcHandler.topic[cid] == 18 then
		if msgcontains(msg, 'yes') then
			if player:removeItem(2152, count[cid]) then
				player:addItem(2148, count[cid] * 100)
				npcHandler:say('Pronto, aqui esta!!', cid)
			else
				npcHandler:say('Desculpe, mas voce nao tem moedas de platinum suficientes.', cid)
			end
		else
			npcHandler:say('Bem, posso ajuda-lo com outra coisa?', cid)
		end
		npcHandler.topic[cid] = 0
	elseif npcHandler.topic[cid] == 19 then
		if getMoneyCount(msg) < 1 then
			npcHandler:say('Desculpe, mas voce nao tem moedas de platinum suficientes.', cid)
			npcHandler.topic[cid] = 0
		else
			count[cid] = getMoneyCount(msg)
			npcHandler:say('Então Voce gostaria que eu mudasse ' .. count[cid] * 100 .. ' of your platinum coins into ' .. count[cid] .. ' crystal coins for you?', cid)
			npcHandler.topic[cid] = 20
		end
	elseif npcHandler.topic[cid] == 20 then
		if msgcontains(msg, 'yes') then
			if player:removeItem(2152, count[cid] * 100) then
				player:addItem(2160, count[cid])
				npcHandler:say('Pronto, aqui esta!!', cid)
			else
				npcHandler:say('Desculpe, mas voce nao tem moedas de platinum suficientes.', cid)
			end
		else
			npcHandler:say('Bem, posso ajuda-lo com outra coisa?', cid)
		end
		npcHandler.topic[cid] = 0
	elseif msgcontains(msg, 'converter crystal') then
		npcHandler:say('Quantas moedas de crystal voce gostaria de transformar em platinum?', cid)
		npcHandler.topic[cid] = 21
	elseif npcHandler.topic[cid] == 21 then
		if getMoneyCount(msg) < 1 then
			npcHandler:say('Desculpe, mas voce nao tem moedas de crystal suficientes.', cid)
			npcHandler.topic[cid] = 0
		else
			count[cid] = getMoneyCount(msg)
			npcHandler:say('Então Voce gostaria que eu mudasse ' .. count[cid] .. ' das suas crystal coins ' .. count[cid] * 100 .. ' em platinum coins para voce?', cid)
			npcHandler.topic[cid] = 22
		end
	elseif npcHandler.topic[cid] == 22 then
		if msgcontains(msg, 'yes') then
			if player:removeItem(2160, count[cid])  then
				player:addItem(2152, count[cid] * 100)
				npcHandler:say('Pronto, aqui esta!!', cid)
			else
				npcHandler:say('Desculpe, mas voce nao tem moedas de crystal suficientes.', cid)
			end
		else
			npcHandler:say('Bem, posso ajuda-lo com outra coisa?', cid)
		end
		npcHandler.topic[cid] = 0
	end
	return true
end

keywordHandler:addKeyword({'dinheiro'}, StdModule.say, {npcHandler = npcHandler, text = 'Nos podemos {trocar} dinheiro para voce. Voce pode tambem acessar a sua {bank account}.'})
keywordHandler:addKeyword({'trocar'}, StdModule.say, {npcHandler = npcHandler, text = 'Existem tres tipos diferentes de moedas no Global Bank: 100 moedas de gold equivalem a 1 moeda de platinum, 100 moedas de platinum equivalem a 1 moeda de crystal. Entao, se voce quiser trocar 100 gold em 1 platinum, simplesmente diga \'{converter gold}\' e então \'1 platinum\'.'})
keywordHandler:addKeyword({'banco'}, StdModule.say, {npcHandler = npcHandler, text = 'Nos podemos {trocar} dinheiro para voce. Voce pode pode tambem acessar a sua {bank account}.'})
keywordHandler:addKeyword({'advanced'}, StdModule.say, {npcHandler = npcHandler, text = 'Sua conta bancaria será usada automaticamente quando Voce quiser {alugar} uma casa ou colocar uma oferta em um item no {mercado}. Deixe-me saber se Voce quer saber como qualquer um deles funciona.'})
keywordHandler:addKeyword({'ajuda'}, StdModule.say, {npcHandler = npcHandler, text = 'Voce pode {trocar} da sua conta bancaria, {depositar} dinheiro ou {retirar} isso. Voce pode tambem {transferir} dinheiro para outros personagens.'})
keywordHandler:addKeyword({'funcoes'}, StdModule.say, {npcHandler = npcHandler, text = 'Voce pode checar a {balance} da sua conta bancaria, {depositar} dinheiro ou {retirar} isso. Voce pode tambem {transferir} dinheiro para outros personagens.'})
keywordHandler:addKeyword({'basic'}, StdModule.say, {npcHandler = npcHandler, text = 'Voce pode checar a {balance} da sua conta bancaria, {depositar} dinheiro ou {retirar} isso. Voce pode tambem {transferir} dinheiro para outros personagens.'})
keywordHandler:addKeyword({'trabalho'}, StdModule.say, {npcHandler = npcHandler, text = 'Eu trabalho neste banco. Posso trocar dinheiro para Voce e ajuda-lo com sua conta bancaria.'})

npcHandler:setMessage(MESSAGE_GREET, "Sim? o que posso fazer por Voce, |PLAYERNAME|? Negocios aqui no banco, que tal?? Se precisar de algo, digite {ajuda}")
npcHandler:setMessage(MESSAGE_FAREWELL, "Tenha um excelente dia, e volte sempre!")
npcHandler:setMessage(MESSAGE_WALKAWAY, "Tenha um excelente dia, e volte sempre!")
npcHandler:setCallback(CALLBACK_GREET, greetCallback)
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
